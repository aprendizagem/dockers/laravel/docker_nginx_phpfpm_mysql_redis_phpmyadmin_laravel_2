# Sobre o código
 * Laravel 6.0
 * nginx:1.17.0-alpine
 * php:7.3-fpm-alpine
 * redis:alpine
 * phpmyadmin:latest
 * Mysql 8.0

# Ordem de comando

Execute o comando para gerar o build da imagem
```
docker-compose up -d --build
```

Uma observação vai demorar pouco pegar tudo, esperar copiar os arquivos do mysql para DBDATA

E não esqueça de dá o comando 
```
chmod -R 777 dbdata
```

Acessar o container da aplicação   
```
docker exec -it nome_container /bin/bash
```
Digite esse comando  
```
cd /var/www/
```
Verifique se o arquivo <strong>.env.example</strong> foi copiado, usando o comando ls -la
Se estiver tudo okay, vai poder  visualizar sua aplicação funcionando.

* PHPMYADMIN -> localhost:8082
* SITE_LARAVEL -> localhost:8081



# DOCKER-COMPOSE.yaml
Essas são configurações que você editar para conectar ao seu banco de dados <br>
e também para configurar o <strong> Mysql </strong>

## configuração do service app
``` 
environment:
      - DB_DATABASE=laravel
      - DB_PASSWORD=root
      - DB_USERNAME=root
      - DB_HOST=db

```
## configuração do service mysql
```
  environment:
      - MYSQL_DATABASE=laravel
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_USER=root
```

## Teste para verificar se o banco de dados se está OKAY
```
   healthcheck:
      test: ["CMD-SHELL", 'mysql -uroot --database=laravel --password=root --execute="SELECT count(table_name) > 0 FROM information_schema.tables;" --skip-column-names -B']
      interval: 10s
      timeout: 10s
      retries: 4
```
Código acima vai verificar se o banco de dado está funcional e está conectando, entretanto, quando você inicia pela primeira vez vai dá erro. Portanto, você digita novamente o comando

```
docker-compose up -d --build
```

Para provisionar o banco de dados demora mais do que outros containers. Contudo, pode alterar o  tempo do <strong> Interval </strong> para que seja de acordo com o tempo que leva para criar o container do <strong> mysql </strong>.

## Configurar  Nginx
#### No arquivo dockerfile do nginx 
```
server {
    listen 80;
    index index.php index.html;
    root /var/www/public;

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass app:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location / {
        try_files $uri $uri/ /index.php?$query_string;
        gzip_static on;
    }
}
```
Atenção nessa parte <strong> fastcgi_pass app:9000; </strong> o nome <strong> app </strong> é do service que está no <strong> docker-compose </strong>, se alterar o nome do service no <strong>docker-compose </strong> é preciso alterar aqui também, porque aqui faz a mágica acontece e parte mais importante, pois é daqui que ele vai redirecionar a sua aplicação para porta 9000
